// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "MLIGamesGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class MLIGAMES_API AMLIGamesGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
