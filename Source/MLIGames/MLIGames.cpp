// Copyright Epic Games, Inc. All Rights Reserved.

#include "MLIGames.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, MLIGames, "MLIGames" );
